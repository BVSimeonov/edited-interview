# Edited-interview


## Description
This scraper uses the scrapy and playwright libraries to load an item from markandspencer's website
(https://www.marksandspencer.com/bg/easy-iron-geometric-print-shirt/p/P60639302.html)
and parse six pieces of information (name, price, default colour, available sizes, reviews' count and score) from it. Then it saves them on a json file, rewriting the previously saved information.

## Task
Using python scrapy framework, you have to retrieve the information about the name,
selected colour, price and size of a single product located at:
https://www.marksandspencer.com/bg/easy-iron-geometric-print-shirt/p/P60639302.html
The solution needs to include the navigation to the product page and extracting the data.
Output of the parsed data needs to be in a json file.
Following steps needs to be implemented:
- request to load the page located at https://www.marksandspencer.com/bg/easy-iron-
geometric-print-shirt/p/P60639302.html
- parse of the html
- collect the data (name, price, selected default colour, size and review count + score)
- output the data as json file, for example:
{
"name": String
"price": Double,
"colour": String,
"size": Array,
"reviews_count": Int,
"reviews_score": Double,
}

## Libraries
Scrapy              2.11.2
playwright          1.44.0

## Installation
To run the scrapes the following instruction should be followed:
1. Create and enter venv
2. pip install scrapy scrapy-playwright playwright
3. playwright install chromium
4. playwright install-deps chromium
5. playwright install

## Usage
Once all preparations and installations are compleated:
1. Use cd command to enter the inner edited folder in the terminal
2. Run the following command in the terminal:
"scrapy crawl spidy"







