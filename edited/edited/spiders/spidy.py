import json
import scrapy

class SpidySpider(scrapy.Spider):
    name = "spidy"

    def start_requests(self):
        url = "https://www.marksandspencer.com/bg/easy-iron-geometric-print-shirt/p/P60639302.html"
        yield scrapy.Request(url, meta={'playwright': True})
            

    def parse(self, response):
        # Check if connection to the website is successful
        if response.status != 200:
            self.logger.error(f"Connection failed {response.url}. Status code: {response.status}")
            return None
    

        name = response.css('h1.product-name::text').get()
        if name: name = name.strip()

        price_string = response.css('span.value::attr(content)').get()
        if price_string:
            try:
                price = float(price_string)
                price = round(price, 2)
            except:
                price = " "
                self.logger.error(f"Failed to parse the price of the item.")

        colour_raw = response.css('div.colour-picker.color-display.swatch-color-P60639302.qa-pdp-color-display.pdp-fit-analytics')
        colour = colour_raw.css('::attr(data-colorname)').get()
        if colour: 
            colour = colour.strip()

        reviews_count_raw = response.css('span.review-summary__total-review.review-summary-count')
        reviews_count = reviews_count_raw.css('span::attr(data-value)').get()
        if reviews_count: 
            try:
                reviews_count = int(reviews_count.strip())
            except:
                reviews_count = " "
                self.logger.error(f"Failed to parse the reviews_count of the item.")

        review_score_raw = response.css('div.bv-review-summary-container')
        reviews_score = review_score_raw.css('div.bv-review-summary::attr(data-rating-value)').get()
        if reviews_score:
            try:
                reviews_score = float(reviews_score.strip())
                rounding_coefficient = 0.05
                reviews_score = round(reviews_score + rounding_coefficient,1)
            except:
                reviews_score = " "
                self.logger.error(f"Failed to parse the reviews_score of the item.")

        list_sizes = response.css('select#plp-select option')
        available_sizes = []
        for i in range(len(list_sizes)):
            is_not_available = list_sizes[i].css('option::attr(data-isoos)').get()
            if is_not_available and is_not_available != "true":
                size = list_sizes[i].css('option::text').get()
                if size: available_sizes.append(size.strip())


        result = {
  	        "name": name,
  	        "price": price,
   	        "colour": colour,
  	        "size": available_sizes,
	        "reviews_count": reviews_count,
	        "reviews_score": reviews_score,
                }
        
        with open('result.json', 'w') as f:
            json.dump(result, f, indent=4)
